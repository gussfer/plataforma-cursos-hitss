module.exports = {
    apps: [{
        name: 'app',
        script: './server.js',
        env: {
            NODE_ENV: 'dev',
        },
        env_prod: {
            NODE_ENV: 'prod',
        },
    }],
};