// Nodemon é um auxiliar que monitora os arquivos da API
const express = require('express'); // Express: framework que facilita o desenvolvimento de aplicações back-end
const cors = require('cors'); // Cors: código que configura API para aceitar requisições de qualquer origem 
const app = express(); // cria uma aplicação do express 
const bodyParser = require('body-parser'); 
const config = require('./config');
app.use(bodyParser.urlencoded({ extended: true })) // traduz e lê objetos  body' da request 
app.use(bodyParser.json()) // traduz e lê objetos 'body' da request
app.use(cors()); // retorna uso do cors
app.use(express.json()); // retorna uso do express
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next();
  
  });
require('./src/Routes/index')(app);
app.listen(config.PORT, () => {
    console.log(`APP LISTENING ON http://${config.PORT}`);
}); // determinando o servidor para carreamento
