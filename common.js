const multer = require('multer'); // importa biblioteca multer
const fs = require('fs'); // importa file system, para leitura e criação de arquivos no sistema
const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const dir = 'C:/Users/Gustavo/Desktop/beck_projeto_hitss_on/uploads/video';
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, {recursive: true});
}
    aws.config.update({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });
const fileStorageEngine = { // metodo diskStorage responsável por implementação
    local: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, dir);
        },
        filename: (req, file, cb) => {
            cb (null, `${Date.now()}-${file.originalname}`); // metodo Date.now() retorna data de upload do arquivo
        },
    }),
    s3: multerS3({
        s3: new aws.S3(),
        bucket: process.env.BUCKET_NAME,
        contentType: multerS3.AUTO_CONTENT_TYPE,
        acl: 'public-read',
        key: (req, file, cb) => {cb(null, `${Date.now()}-${file.originalname}`);},
    }),
};
const upload = multer({storage: fileStorageEngine[process.env.STORAGE_TYPE]});
module.exports = upload;

