// Módulo que é responsável por controlar a maneira como um usuário interage com uma aplicação //
// É o controlador que determina que resposta será enviada de volta ao usuário quando ele faz uma requisição. //

const { json } = require("express/lib/response"); // ???
const db = require("../../db"); // defindo banco de dados
const bcrypt = require('bcrypt'); // biblioteca para criptografar senha
const fs = require('fs'); // importa file system, para leitura e criação de arquivos no sistema
const pdf = require('html-pdf');
const aws = require('aws-sdk')

// CRUD: métodos padrão do protocólo HTTP.
// C – Create (POST); R – Read (GET); U – Update (PUT); D – Delete (DELETE)

// operação post: adiciona informações ao servidor 
exports.post = async (req, res, next) => // req, res, next são atributos básicos de toda requisição
{ 
    const {body} = req;
    const courseData =  // define os dados que serão inseridos
        {
            Title: body.Title,
            Course_Resume: body.Course_Resume,
        }
        db("course") // sintaxe knex para selecionar o db // db.select().table("courses") - similar
        .insert(courseData) // sintaxe knex para inserção no db
        .then((data => // sintaxe knex para interação com db
        {
            //console.log(data) //printa o retorno no termnal
            res.status(201).send( //retorna 
                {
                ...courseData,
                id: data[0],
                });
        }))
};

// operação put:  altera um recurso existente no servidor 
exports.put = async (req, res, next) => 
{ 
    const {id} = req.params;
    await db("course").update(req.body).where({"id_course": id});
    const updateCourse = await db("course").where({"id_course": id});
    return res.status(200).json(...updateCourse);
};

// operação delete
exports.delete = async (req, res, next) => 
{ 
    const id = req.params.id;
    if (! await db("course").where({ // validação caso usuário não seja encontrado
        "id_course": id})
        .first())
        {
        return res.status(400).json({ error: "Curso não existe"}) // validação caso usuário não seja encontrado
        };
    db.select().table("course").del().where({"id_course": id}).then(() => {
        return res.status(200).json({ message: 'Curso deletado'});

})
};

// operação get: retorna informação do servidor 
exports.get = (req, res, next) => { 
    db("course").then(data => { 
    res.status(200).send(data) // retorna os dados em formato de lista
        }
    )
};

// operação getByID:retorna informação específica do servidor (baseada no ID)
exports.getById = (req, res, next) => { 
    let id = req.params.id;
    db("course").where({ 
        "id_course": id})
        .then((data) => {
        if (data.length == 0) { // se retornar lista vazia,
          return res.status(400).json({ error: "Curso não existe"});
        } else { // se não retornar lista vazia, 
            res.status(200).send(data); // retorna os dados em formato de lista
        }
    })
    
};

// Busca de aulas por curso
exports.getLessonList = (req, res) => {
    const {id_course} = req.params;
    db('aulas_courses').where({id_course}).then((data) => {
        if (data.length == 0) {
            return res.status(400).json({error: 'Curso inexistente'})
        }
        const sortData = data.sort((a, b) => {
            a.number >= b.number ? 1 : -1 // Operador Condicional Ternário para ordenar de forma crescente a lista
        });
        return res.status(200).send(sortData);
    });
};

// Busca de aula específica
exports.getLesson = (req, res) => {
    const {id_course, lessonNumber} = req.params;
    db('aulas_courses').where({id_course, number: lessonNumber}).first().then((data) => {
        if (! data || data.length == 0) {
            return res.status(400).json({error: 'Curso inexistente'})
        }
        return res.status(200).send(data);
    });
};

// Criação de uma aula nova
exports.createLesson = (req, res) => {
    const {body} = req; 
    db('aulas_courses').insert({... body, id_lesson: `${body.id_course}-${body.number}`}).then((data) => {
        res.status(201).send({
            ...body,
            id_lesson: `${body.id_course}-${body.number}`,
        });
    });
};

// Delete Lesson
exports.deleteLesson = async (req, res, next) => 
{ 
    const {id_course, lessonNumber} = req.params;
    if (! await db("aulas_courses").where({id_course, number: lessonNumber}).first())
        {
        return res.status(400).json({ error: "Aula não existe"}) // validação caso usuário não seja encontrado
        };
    db.select().table("aulas_courses").del().where({id_course, number: lessonNumber}).then(() => {
        return res.status(200).json({ message: 'Aula deletada'});

})
};
// Edit Lesson
exports.putLesson = async (req, res, next) => 
{ 
    const {body} = req;
    const {id_course, lessonNumber} = req.params;
    if (! await db("aulas_courses").where({id_course, number: lessonNumber}).first())
        {
        return res.status(400).json({ error: "Aula não existe"}) // validação caso usuário não seja encontrado
        };

    await db("aulas_courses").update(body).where({id_course, number: lessonNumber});
    const updateCourse = await db("aulas_courses").where({id_course, number: lessonNumber});
    return res.status(200).json(...updateCourse);
};

// Upload de vídeos
exports.videoUpload = async (req, res) => {
    const {id_course, lessonNumber} = req.params;
    console.log(req.file);
    console.log('Done');
    await db('aulas_courses').where({id_course, number: lessonNumber}).first().update({video_path: req.file.path || req.file.key});
    res.send("Upload completo")
};
// Uso do modulo Stream (Node.js) para streaming das aulas
exports.getVideo = async (req, res) => {
    const {id_course, lessonNumber} = req.params;
    const movieFile = await db('aulas_courses').where({id_course, number: lessonNumber}).first();
    if (!movieFile || !movieFile.video_path) {return res.status(404).end('<h1>Video não encontrado</h1>');}
    if (process.env.STORAGE_TYPE == 's3') 
    {
        const s3 = new aws.S3();
        aws.config.update({
            acessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAcessKey: process.env.AWS_SECRET_ACESS_KEY,
        });
        const url = s3.getSignedUrl('getObject', {
            Bucket: process.env.BUCKET_NAME,
            Key: movieFile.video_path,
            Expires: 36000000000000*10,
        });
        
        console.log(url);
        return res.status(200).send(url);
    }
    fs.stat(movieFile.video_path, (err, stats) => { // fs stat retorna status do caminho do video no dir
        if (err) {return res.status(404).end('<h1>Video não encontrado</h1>');}
    // Variáveis necessárias para montar o Chunk header corretamente
    const {range} = req.headers;
    const {size} = stats;
    const start = Number ((range || '').replace(/bytes=/, '').split('-')[0]); // tratamento no renge inserido na requisição
    const end = size - 1;
    const chunkSize = (end - start) + 1;
    // Defindo os headers de chunk
    res.set({
        'Content-Range': `bytes ${start}-${end}/${size}`,
        'Accept-Ranges': 'bytes',
        'Content-Lenght': chunkSize,
        'Content-Type': 'video/mp4',
    });
    // Usar status 206 - partial content para streaming funcionar 
    res.status(206);
    // Com o ReadStream do Node é possivel ler e enviar um arquivo em partes via stream.pipe()
    const stream = fs.createReadStream(movieFile.video_path, {start, end}); // stream para "ler" o video
    stream.on('open', () => stream.pipe(res)); // pipe abre o caminho para as requisições parciais
    stream.on('error', (streamErr) => res.end(streamErr));
    return null;
    });
    return null;
};
// Checagem status do aluno em relação ao curso
exports.checkStatus = (req, res) => {
    const {course_id, user_id} = req.params;
    const status = [
        {code: 0, status: 'Não registrado'},
        {code: 1, status: 'Cursando'},
        {code: 2, status: 'Concluido'}
    ];
    db('course_status').where({course_id, user_id}).first().then((data) =>{
        if (!data) {
            return res.status(200).send(status[0]); 
        }
        return res.status(200).send(status[data.status]);
    });
};

// Adicionar aluno no curso
exports.addToCourse = async (req, res) => {
    const {course_id, user_id} = req.params;
    const data = await db('course_status').where({course_id, user_id}).first();
    if (!data) {
        console.log('criado');
        await db('course_status').insert({
            course_id, user_id, status: 1,
        });
    } else {
        await db('course_status').where({course_id, user_id}).first().update({status: 1});
    }
    return res.status(200).send('Usuário adicionado ao curso')
};

//Mudar status do curso para concluido
exports.setComplete = async (req, res) => {
    const {course_id, user_id} = req.params;
    const {body} = req;
    const data = await db('course_status').where({course_id, user_id}).first();
    if (!data) {
        return res.status(400).send('Usuário não está neste curso')
    }
    await db('course_status').where({course_id, user_id}).first().update({completionDate: body.completionDate, status: 2});
    return res.status(200).send('Usuário completou o curso')
};

// Checagem de visualização de aula
exports.checkProgess = (req, res) => {
    const {course_id, user_id} = req.params;
    db('progress').where({course_id, user_id}).first().then((data) =>{
        if (!data) {
            return res.status(200).send({lastSeen: 0}); // last seen = 0 - nenhuma aula visualizada
        }
        return res.status(200).send({lastSeen: data.lastSeen || 0});
    })
};

// Atualização de progresso na visualização da aula
exports.updateProgress = async(req, res) => {
    const {course_id, user_id} = req.params;
    const {body} = req;
    const data = await db('progress').where({course_id, user_id}).first();
    if (!data) {
        console.log('criado');
        await db('progress').insert({
            course_id, user_id, lastSeen: body.lastSeen, id_progress: `${user_id}-${course_id}`,
        });
    } else {
        await db('progress').where({course_id, user_id}).first().update({lastSeen: body.lastSeen});
    }
    return res.status(200).send('Progresso atualizado')
};

// Geração de certificado em pdf
exports.gerarPdf = async (req, res) => {
    const {course_id, user_id} = req.params;
    const course = await db('course').where({id_course: course_id}).first(); // seleciona o curso associado ao da requisição
    const user = await db('users').where({id_users: user_id}).first(); // seleciona o usuário associado ao da requisição
    const html = `
    
    <style>
    .certificado {
      background-color: rgb(6, 88, 143);
      color: rgb(3, 201, 13);;
      padding: 1px;
      text-align: center;
      font-family: Arial;
      font-size: 32px;
      margin: -8px;
    } 
    .corpo {
        color: black;
        padding: 100px;
        text-align: center;
        font-family: Arial;
        font-size: 20px;
    }
    .vazia {
        padding: 40px;
    }
    </style>
    
    <body>
        <div class="certificado">
            <h2>HITSS ON</h2></div> 
        <div class="vazia"></div>
        <div class="corpo">
            <h2><b>Certificamos que ${user.first_name} ${user.last_name}</b> concluiu o curso <b>${course.Title}</b> com sucesso</h2>
            <p>Total de horas: <b>80</b></p></div>
        <div class="vazia"></div>
        <div class="certificado">
            <h2>CAPACITA TI</h2>
        </div>
    </body>`;
    const options = {
        type: 'pdf',
        format: 'A4',
        orientation: 'landscape',
    };
    pdf.create(html, options).toBuffer((err, buffer) => {
        if (err) return res.status(500).json(err);
        return res.end(buffer)
    });

}