// Módulo que é responsável por controlar a maneira como um usuário interage com uma aplicação //
// É o controlador que determina que resposta será enviada de volta ao usuário quando ele faz uma requisição. //

const db = require("./../../db"); // defindo banco de dados
const bcrypt = require('bcrypt'); // biblioteca para criptografar senha
const jwt = require("jsonwebtoken");
const { json } = require("express/lib/response");
const nodemailer = require ("nodemailer");


// CRUD: métodos padrão do protocólo HTTP.
// C – Create (POST); R – Read (GET); U – Update (PUT); D – Delete (DELETE)

// operação post: adiciona informações ao servidor 
exports.post = async (req, res, next) => // req, res, next são atributos básicos de toda requisição
{ 
    const {body} = req;
    const hash = await bcrypt.hashSync(body.password, 10) // criação objeto para criptografar
    const userData =  // define os dados que serão inseridos
        {
            first_name: body.first_name,
            last_name: body.last_name,
            email: body.email,
            cpf: body.cpf,
            phone: body.phone,
            password: hash,
        }
    db("users") // sintaxe knex para selecionar o db // db.select().table("users") - similar
    .insert(userData) // sintaxe knex para inserção no db
    .then((data) => 
    {
        console.log(userData) //printa o retorno no termnal
        res.status(201).send('Usuário criado com sucesso');
    })
};

// operação put:  altera um recurso existente no servidor 
exports.put = async (req, res, next) => 
{ 
    const id = req.params.id;
    let userData = { // define os dados que serão inseridos
        ... req.body,}
    if (! await db("users").where({ // validação caso usuário não seja encontrado
        "id_users": id})
        .first())
        {
        return res.status(400).json({ error: "usuário não existe"}) // validação caso usuário não seja encontrado
        };

    if (req.body.password) 
    {
        const hash = await bcrypt.hashSync(req.body.password, 10)
        userData.password = hash;
    }
    await db("users").update(userData).where({"id_users": id});
    const updateUser = await db("users").where({"id_users": id});
    return res.status(200).json("Usuário alterado com sucesso");

};

// operação delete
exports.delete = async (req, res, next) => 
{ 
    const id = req.params.id;
    if (! await db("users").where({ // validação caso usuário não seja encontrado
        "id_users": id})
        .first())
        {
        return res.status(400).json({ error: "usuário não existe"}) // validação caso usuário não seja encontrado
        };
    db.select().table("users").del().where({"id_users": id}).then(() => {
        return res.status(200).json({ message: 'Usuário deletado'}) 
    })
};

// operação get: retorna informação do servidor 
exports.get = (req, res, next) => { 
    db("users").then(data => 
        {
        res.status(200).send(data) // retorna os dados em formato de lista
        }
    )
};

// operação getByID:retorna informação específica do servidor (baseada no ID)
exports.getById = (req, res, next) => { 
    let id = req.params.id;
    db("users").where({ // sintaxe knex para interação com db
        "id_users": id})
        .first()
        .then((data) => {
        if (!data){ 
            return res.status(400).json({ error: "usuário não existe"}); // retorna erro 400 e mensagem de erro
        }   else {
            res.status(200).send(data); // retorna os dados em formato de lista

        }
    })
    
};

// Geração de token no login - Json Web Token(JWT)
exports.login = async (req, res, next) => 
{
    const {email, password} = req.body; // definindo que precisam ser enviados email e senha no body da req
    if (!email || !password) 
    return res.status(400).send({Erro: 'Campos inválidos'})

    const user = await db.select().table("users")
    .select('password')
    .where('email', email) // consuta ao bd onde a senha pertence ao email do body
    .first(); 
    if (!user) // checa se email está no bd
    return res.status(404).send({Erro: "Usuário não encontrado"});
    if (!await bcrypt.compareSync(password, user.password)) // comparação de senha contida no body com a senha do bd
    {
        return res.status(401).send({error: 'Senha inválida'});
    }

    const loggedUser = await db.select().table("users") // consulta ao bd
    .select("*") // de todos os dados
    .where('email', email) // do email contido no body
    .first();
    delete loggedUser.password // deleção da senha do user para retorno 

    const token = jwt.sign({ // metodo sign: padrão para geração de jwt
        user: loggedUser.id}, 
        process.env.JWT_SECRET,
        {expiresIn: 300});

    return res.status(200).send(
        {user: {...loggedUser}, token});
};

// Recuperação de senha via email | token \\

const transport = nodemailer.createTransport({ // inbox para envio do email
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "467d26df2a26b9",
      pass: "4f63901aa7a1e3"
          }
    });

const passwordToken = ({ password: 
    passwordHash, id_users,}) => {
    const secret = `${passwordHash}-${id_users}`;
    const token = jwt.sign({ id_users }, secret, {expiresIn: 3600,}); // gerando o token
    return token;
};
// envio de email para reset de senha
exports.passwordResetEmail = async (req, res) => {
    const{email} = req.params;
        
    const user = await db('users').select('*').where('email', email).first(); // definindo usuário com email da requisição
    if (!user) 
        {
        return res.status(404).json('Nenhum usuário com este email');
        }

    const token = passwordToken(user); // relacionando o usuário com a senha encriptada
    const mailOptions = {
    to: email, 
    subject: 'Recuperação de senha',
    text: `Link de recuperação: https://url/reset/${user.id_users}/${token}`,
    };
    
    const sendEmail = () => {
        transport.sendMail(mailOptions, (err, data) => {
        if (err) {
            console.log(`Error ${err}`);
            res.status(404).json('Failed');
        } else { 
            console.log('Email enviado com sucesso', data.response);
            res.status(200).json('Enviado');
            }
    });
};
sendEmail();
};

exports.newPassword = async (req, res) => 
{
    const {id, token} = req.params;
    const {password} = req.body;

    const user = await db('users').select('*').where('id_users', id).first();
    const secret = `${user.password}-${id}`;
    const payload = jwt.decode(token, secret);
    if (payload.id == user.id) {
        const hash = await bcrypt.hashSync(password, 10);
        db('users').update({password: hash}).where({'id_users': id}).then(() => 
        res.status(202).json('Senha alterada com sucesso'));
    } //else {res.status(404).json('Usuário inválido');}
};
