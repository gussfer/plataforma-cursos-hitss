// Rotas referentes a usuario
const fs = require('fs');
const coursesController = require('../Controllers/coursesController');
const upload = require('../../common');
module.exports = (app) => {
    app.get('/', (req, res) => {
        fs.readFile('./index.html', (err, html) => res.end(html));
    });
    
    app.post('/course', coursesController.post); // rota para chegar no course Controller, no caso criação de usuário
    app.post('/course/video', coursesController.createLesson); // rota post para criar uma aula nova, adicionando as informações da mesma
    app.put('/course/:id', coursesController.put); // rota para chegar no course Controller, no caso alteração de usuário
    app.delete('/course/:id', coursesController.delete); // rota para chegar no course Controller, no caso deleção de usuário
    app.get('/courses', coursesController.get); // rota para chegar no course Controller, no caso retorno de usuários
    app.get('/course/:id', coursesController.getById); // rota para chegar no course Controller, no caso retorno de usuário específico
    app.get('/courses/list/:id_course', coursesController.getLessonList); // rota para retornar lista de aulas por curso
    app.get('/courses/video/:id_course/:lessonNumber', coursesController.getLesson); // rota para retornar aula solicitada em curso solicitado
    app.post('/courses/upload/:id_course/:lessonNumber', upload.single('video'), coursesController.videoUpload);
    app.get('/course/:id_course/:lessonNumber', coursesController.getVideo);
    app.put('/course/:id_course/:lessonNumber', coursesController.putLesson);
    app.delete('/course/:id_course/:lessonNumber', coursesController.deleteLesson);
    app.get('/courses/progress/:course_id/:user_id', coursesController.checkProgess);
    app.post('/courses/progress/:course_id/:user_id', coursesController.updateProgress);
    app.get('/courses/status/:course_id/:user_id', coursesController.checkStatus);
    app.post('/courses/status/:course_id/:user_id', coursesController.addToCourse);
    app.post('/courses/statusComplete/:course_id/:user_id', coursesController.setComplete);
    app.get('/courses/pdf/:course_id/:user_id', coursesController.gerarPdf);
}