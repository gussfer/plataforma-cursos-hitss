// Exportar rotas a unir em único arquivo 

const courseRoute = require('./courseRoute');
const userRoute = require('./userRoute');
module.exports = (app) => {
    userRoute(app)
    courseRoute(app)
}