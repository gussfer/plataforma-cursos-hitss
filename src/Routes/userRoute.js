// Rotas referentes a usuario

const userController = require('../Controllers/userController');
module.exports = (app) =>
 {
    app.post('/user', userController.post); // rota para chegar no User Controller a requisição tipo post
    app.put('/user/:id', userController.put); // rota para chegar no User Controller a requisição put 
    app.delete('/user/:id', userController.delete); // rota para chegar no User Controller a requisição delete
    app.get('/users', userController.get); // rota para chegar no User Controller a requisição get
    app.get('/user/:id', userController.getById); 
    app.post('/user/login', userController.login);
    app.post('/user/:email', userController.passwordResetEmail);
    app.post('/user/reset/:id/:token', userController.newPassword);
};
